local _, L = ...;
if GetLocale() == "enGB" or GetLocale() == "enUS" then
 
	-- Common messages
	L["desc"] 		= "This addon plays sounds for each chat channel.";
	L["close"] 		= "Close";
	L["whisper"]	= "Whisper";
	L["guild"]		= "Guild";
	L["raid"]		= "Raid";
	L["party"]		= "Party";
	L["offi"]		= "Officer";
	
end