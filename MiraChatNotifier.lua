local _, L = ...;

function MiraChatNotifier_OnLoad()

	-- Register events
	MiraChatNotifier:RegisterEvent("VARIABLES_LOADED");
	MiraChatNotifier:RegisterEvent("ADDON_LOADED");

	MiraChatNotifierEventMap = {};
	MiraChatNotifierEventMap["CHAT_MSG_BN_WHISPER"] 					= "whisper";
	MiraChatNotifierEventMap["CHAT_MSG_GUILD"] 								= "guild";
	MiraChatNotifierEventMap["CHAT_MSG_RAID"] 								= "raid";
	MiraChatNotifierEventMap["CHAT_MSG_RAID_LEADER"] 					= "raid";
	--MiraChatNotifierEventMap["CHAT_MSG_BATTLEGROUND"] 				= "raid";
	--MiraChatNotifierEventMap["CHAT_MSG_BATTLEGROUND_LEADER"] 	= "raid";
	MiraChatNotifierEventMap["CHAT_MSG_PARTY"] 								= "party";
	MiraChatNotifierEventMap["CHAT_MSG_PARTY_LEADER"] 				= "party";
	MiraChatNotifierEventMap["CHAT_MSG_OFFICER"] 							= "offi";
	MiraChatNotifierEventMap["CHAT_MSG_WHISPER"] 							= "whisper";

	for key,value in pairs(MiraChatNotifierEventMap) do
		MiraChatNotifier:RegisterEvent(key);
	end

	MiraChatNotifierControlPrefix = "MiraChatNotifier";
	MiraChatNotifierLoaded = false;

	MiraChatNotifierSettings 		= {};
	MiraChatNotifierControls 		= {};
	MiraChatNotifierComboBoxes 	= {};

	MiraChatNotifierSounds = {};

	for za=1,11 do
		table.insert(MiraChatNotifierSounds, za .. ".ogg");
	end

end

function MiraChatNotifier_OnEvent(self, event, ...)

	local arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17, arg18 = select(1, ...)

	if MiraChatNotifierLoaded == false then

		if event == "VARIABLES_LOADED" then
			MiraChatNotifier_Write("VARIABLES_LOADED is used to load the settings");
			MiraChatNotifier_VariablesLoaded();
			MiraChatNotifierLoaded = true;
		end

		if event == "ADDON_LOADED" and arg1 == "MiraChatNotifier" then
			MiraChatNotifier_Write("ADDON_LOADED is used to load the settings");
			MiraChatNotifier_VariablesLoaded();
			MiraChatNotifierLoaded = true;
		end

	end

	if MiraChatNotifierLoaded == false then
		return;
	end

	local channel = MiraChatNotifierEventMap[event];

	if(channel ~= nil) then
		if(MiraChatNotifierSettings[channel]["active"] == true) then
			PlaySoundFile("Interface\\AddOns\\MiraChatNotifier\\Sounds\\" .. MiraChatNotifierSettings[channel]["sound"], "Master");
		end
	end

end

function MiraChatNotifier_CreateCheckBox(name, caption, x, y, parent)

	local control = CreateFrame("CheckButton", MiraChatNotifierControlPrefix .. name, parent, "InterfaceOptionsCheckButtonTemplate")
	_G[control:GetName() .. "Text"]:SetText(caption);
	control:SetScript("OnClick", MiraChatNotifier_ChatOptionChanged)
	control:SetPoint("TOPLEFT", x, y);
	MiraChatNotifierControls[name] = control;

	MiraChatNotifier_CreateComboBox(name, control, parent);

end

function MiraChatNotifier_CreateComboBox(name, control, parent)

	local menuFrame = CreateFrame("Frame", MiraChatNotifierControlPrefix .. name .. "combobox", parent, "UIDropDownMenuTemplate")
	menuFrame:SetPoint("RIGHT", control, 200, 0);
	MiraChatNotifierComboBoxes[name] = menuFrame;

	UIDropDownMenu_Initialize(menuFrame, MiraChatNotifier_CreateComboBoxItems)

	UIDropDownMenu_SetWidth(menuFrame, 75);
	UIDropDownMenu_SetButtonWidth(menuFrame, 124)
	UIDropDownMenu_SetSelectedID(menuFrame, 1)
	UIDropDownMenu_JustifyText(menuFrame, "LEFT")
	UIDropDownMenu_EnableDropDown(menuFrame);

end

function MiraChatNotifier_CreateComboBoxItems(self, level)

	local menuFrame = _G["UIDROPDOWNMENU_INIT_MENU"];
	local info = UIDropDownMenu_CreateInfo()

	for k,v in pairs(MiraChatNotifierSounds) do
		info = UIDropDownMenu_CreateInfo()
		info.text = v
		info.func = MiraChatNotifier_OnComboBoxChange
		info.arg1 = menuFrame
		UIDropDownMenu_AddButton(info, level)
	end
end

function MiraChatNotifier_OnComboBoxChange(self, arg1, arg2, checked)

	local menuFrame = arg1
	if(menuFrame ~= nil) then
		UIDropDownMenu_SetSelectedID(menuFrame, self:GetID())

		-- cut the control prefix
		local length 		= string.len(MiraChatNotifierControlPrefix);
		local channel 	= string.sub(menuFrame:GetName(), length + 1);

		-- cut the combobox postfix
		local combolen  = string.len("combobox");
		local remainlen = string.len(channel);
		channel					= string.sub(channel, 0, remainlen-combolen);

		local selectedItem = UIDropDownMenu_GetText(menuFrame);

		MiraChatNotifierSettings[channel]["sound"]  = selectedItem;

		PlaySoundFile("Interface\\AddOns\\MiraChatNotifier\\Sounds\\" .. MiraChatNotifierSettings[channel]["sound"], "Master");

		MiraChatNotifier_Write("OnComboBoxChange for [" .. channel .. "]: " .. MiraChatNotifier_SettingToString(MiraChatNotifierSettings[channel]));

	else
		MiraChatNotifier_Write("menuFrame is nil");
	end

end

function MiraChatNotifier_ChatOptionChanged(sender, mousebutton)

	local length 	= string.len(MiraChatNotifierControlPrefix);
	local channel = string.sub(sender:GetName(), length + 1);

	local checked = sender:GetChecked();

	MiraChatNotifierSettings[channel]["active"] = checked;
	MiraChatNotifier_Write("ChatOptionChanged for [" .. channel .. "]:" .. MiraChatNotifier_SettingToString(MiraChatNotifierSettings[channel]));

end

function MiraChatNotifier_CreateChatOptionIfNeeded(name, sound)

	if(MiraChatNotifierSettings[name] == nil) then
		MiraChatNotifier_Write("Settings for [".. name .."] are empty - using default");
		MiraChatNotifierSettings[name] = {};
		MiraChatNotifierSettings[name]["active"] = true;
		MiraChatNotifierSettings[name]["sound"]  = sound;
	else
		MiraChatNotifier_Write("Settings for [".. name .."] loaded: " .. MiraChatNotifier_SettingToString(MiraChatNotifierSettings[name]));
	end

	-- check if the selected sound file is a mp3, if so, fallback to default (since patch 6.0.2 PlaySoundFile seems to play only ogg files correctly)
	local savedSound = MiraChatNotifierSettings[name]["sound"];
	if(strfind(savedSound, ".mp3") ~= nil) then
		MiraChatNotifier_Write("Have to replace the mp3 with ogg");
		local res = string.gsub(savedSound, ".mp3", ".ogg")
		MiraChatNotifierSettings[name]["sound"]  = res;
		MiraChatNotifier_Write("Result: " .. MiraChatNotifier_SettingToString(MiraChatNotifierSettings[name]));
	end


end

function MiraChatNotifier_UpdateDialog()

	MiraChatNotifier_Write("UpdateDialog")

	-- update dialog
	for key,value in pairs(MiraChatNotifierControls) do

		local checked = MiraChatNotifierSettings[key]["active"];
		value:SetChecked(checked);

		MiraChatNotifier_Write("Update for [" .. key .. "]: " .. MiraChatNotifier_SettingToString(MiraChatNotifierSettings[key]));

	end

	for key,value in pairs(MiraChatNotifierComboBoxes) do

		local ctrlName = value:GetName();
		local sound    = MiraChatNotifierSettings[key]["sound"];

		-- open, update and then close combo box to ensure that the items are correctly updated
		ToggleDropDownMenu(1, nil, value, nil, 0, 0);
		UIDropDownMenu_SetSelectedName(value, MiraChatNotifierSettings[key]["sound"], true)
		ToggleDropDownMenu(1, nil, value, nil, 0, 0);
	end

	MiraChatNotifier_Write("UpdateDialog done")

end

function MiraChatNotifier_VariablesLoaded()

  local f = CreateFrame("Frame", nil, nil, InterfaceOptionsFrame);
  local category, layout = Settings.RegisterCanvasLayoutCategory(f, "MiraChatNotifier");
  Settings.RegisterAddOnCategory(category);

	f:SetScript("OnShow", MiraChatNotifier_UpdateDialog)

	local title = f:CreateFontString("ARTWORK")
	title:SetFontObject("GameFontNormal")
	title:SetPoint("TOP", 0, -14)
	title:SetText("MiraChatNotifier")

	local desc = f:CreateFontString("ARTWORK")
	desc:SetFontObject("GameFontHighlight")
	desc:SetJustifyV("TOP")
	desc:SetJustifyH("LEFT")
	desc:SetPoint("TOPLEFT", 18, -32)
	desc:SetPoint("BOTTOMRIGHT", -18, 48)
	desc:SetText(L["desc"]);

	MiraChatNotifierSettings = _G["MiraChatNotifierSettings"];

	if MiraChatNotifierSettings == nil then
		MiraChatNotifier_Write("VariablesLoaded: MiraChatNotifierSettings is nil");
	end

	MiraChatNotifier_CreateChatOptionIfNeeded("whisper", 	"1.ogg");
	MiraChatNotifier_CreateChatOptionIfNeeded("guild", 		"2.ogg");
	MiraChatNotifier_CreateChatOptionIfNeeded("raid", 		"3.ogg");
	MiraChatNotifier_CreateChatOptionIfNeeded("party", 		"4.ogg");
	MiraChatNotifier_CreateChatOptionIfNeeded("offi", 		"5.ogg");

	MiraChatNotifier_CreateCheckBox("whisper", 	L["whisper"], 28, -70, f);
	MiraChatNotifier_CreateCheckBox("guild", 		L["guild"], 	28, -100, f);
	MiraChatNotifier_CreateCheckBox("raid", 		L["raid"], 		28, -130, f);
	MiraChatNotifier_CreateCheckBox("party", 		L["party"], 	28, -160, f);
	MiraChatNotifier_CreateCheckBox("offi", 		L["offi"], 		28, -190, f);

end

function MiraChatNotifier_Write(msg)

	DEFAULT_CHAT_FRAME:AddMessage("[MiraChatNotifier] " .. msg);

end

function MiraChatNotifier_BoolToString(bool)

	msg = "true"

	if(bool ~= true) then
		msg = "false"
	end

	return msg;

end

function MiraChatNotifier_SettingToString(setting)

	return "Active: [" .. MiraChatNotifier_BoolToString(setting["active"]) .. "] Sound: [".. setting["sound"] .. "]";

end
