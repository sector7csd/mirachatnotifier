## Interface: 110000
## Title : MiraChatNotifier
## Notes: This AddOn plays sounds when receiving a chat message.
## Notes-deDE: Spielt Sounds wenn man eine Chat-Nachricht erhält.
## Author: Dorijana (aka Miralu), EU-Antonidas (Sector7CSD)
## Version: 1.1.0
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: MiraChatNotifierSettings
MiraChatNotifier.xml
MiraChatNotifier.lua
localization.core.lua
localization.de.lua
localization.en.lua
