local _, L = ...;
if GetLocale() == "deDE" then

	-- Common messages
	L["desc"] 		= "Dieses Addon spielt Sounds für die verschiedenen Chatchannels.";
	L["close"] 		= "Schließen";
	L["whisper"]	= "Whisper";
	L["guild"]		= "Gilde";
	L["raid"]		= "Raid";
	L["party"]		= "Gruppe";
	L["offi"]		= "Offi";

end